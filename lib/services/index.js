
module.exports = {
    Friends: require("./Friends"),
    User: require("./User"),
    Trivia: require("./Trivia"),
    Admin: require("./Admin"),
    Config: require("./Config"),
    Activity: require("./Activity"),
    Campaign: require("./Campaigns"),
    Cart: require("./Cart"),
    Invoice: require("./Invoice"),
    Item: require("./Item"),
    Challenge: require('./Challenges'),
    Reward: require('./Rewards'),
    Notification: require('./Notifications'),
    Maintenance: require('./Maintenance'),
    Entitlement: require('./Entitlement'),
    Wallet: require('./Wallet'),
    Payment: require('./Payment'),
    Video: require('./Video'),
    Disposition: require('./Disposition'),
    Batch: require('./Batch'),
    Event: require('./Event')
};